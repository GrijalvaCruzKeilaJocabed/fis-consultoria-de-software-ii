# **:computer: Consultorias de Software en Mexico :computer:**
## :one: TANGO
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/tango.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***TANGO***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][una] 

[una]: https://tango.io/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en  Av La Paz 51, Sta Bárbara, 28017 Colima, Col.
[Ir a la ubicacion en google maps][google].

[google]: https://goo.gl/maps/f4jY9M5CQirSNUzX8

### :large_blue_circle: **`Acerca de:`** :question:
> Tango (anteriormente TangoSource) es la principal consultora de desarrollo de productos nearshore y aumento de personal.
¡Creemos en el poder de las ideas innovadoras y la pasión por el producto! Nos enorgullecemos de mantener una cultura diversa y acogedora con un fuerte enfoque en la honestidad, la transparencia y el respeto mutuo. Nos esforzamos por superar no solo las expectativas de nuestros clientes, sino también las expectativas del usuario final.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Producto mínimo viable
- Aumento del personal
- Resolución de deuda técnica
- Estrategia de producto
- Escalabilidad y aceleración del producto
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:** https://web.facebook.com/Tango.ioMX]

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/tango-io/

:small_orange_diamond: **Twitter:** https://twitter.com/tango_io

:small_orange_diamond: **Instagram:** https://www.instagram.com/tangodotio/

:small_orange_diamond: **Correo:** contact@tango.io

### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de tango, se pueden encontrar en el siguiente enlace:
[https://jobs.lever.co/tango](https://jobs.lever.co/tango)
### :large_blue_circle: **`Blog:`** :pencil:
El blog de Tango para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
 [https://blog.tango.io/](https://blog.tango.io/)
### :large_blue_circle: **`Tecnologia:`** :calling:
:small_orange_diamond: **DevOps**
- Jenkins
- Terraform
- Estibador
- Heroku

:small_orange_diamond: **WebDev**
- JavaScript
- HTML
- CSS
- Nodo
- Ruby on Rails

:small_orange_diamond:**Movil**
- Java
- Kotlin
- C Objetivo

:small_orange_diamond:**SaaS**
- Edificio MVP
- Pronostico de versiones
- Planificaciones de la cartera de productos

:small_orange_diamond: **UX / UI**
- Adobe XD
- Photoshop
- Ilustrador
- Invisión

:small_orange_diamond: **Comercio electrónico**
- Shopify
- Raya
- Magento
- Solidus

:small_orange_diamond: **Sin servidor**
- AWS Lambda
- Google Cloud
- Marco sin servidor

:small_orange_diamond: **Seguridad**
- Guardia
- Inspector de AWS
- Cloudwatch

## :two: NEARSOFT
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/nearsoft.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***NEARSOFT***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][dos] 

[dos]: https://nearsoft.com
### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google1].

[google1]: https://goo.gl/maps/9P3a1bkDbyYzyPmr6

### :large_blue_circle: **`Acerca de:`** :question:
> Nearsoft lo ayuda a hacer crecer su equipo de desarrollo con un equipo dedicado de +300 ingenieros en México. Terminas con un equipo integrado, donde todos hablan el mismo idioma, comparten los mismos valores y se levantan y se van a dormir al mismo tiempo. Las empresas acuden a nosotros porque están: 1. Desilusionadas con la "deslocalización". 2. Frustrados porque no pueden escalar su equipo de desarrollo de software. 3. Cansado de lanzamientos tardíos de productos y de mala calidad. 
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo de productos de software
- Experiencia de usuario y diseño interactivo
- innovación
- Software, diseño de UX, desarrollo de iOS
- Desarrollo de Android, ingeniería de software, codificadores, desarrollo de software
- Innovacion tecnologica
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/Nearsoft/?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/nearsoft/

:small_orange_diamond: **Twitter:** https://twitter.com/Nearsoft

:small_orange_diamond: **Youtube:** https://www.youtube.com/channel/UCdNFp0oJcRblD0qPOVq3xZw/videos

### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de nearsoft, se pueden encontrar en el siguiente enlace:
https://nearsoft.com/join-us/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de nearsoft para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://nearsoft.com/blog/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Java 
- Ruby
- Python
- Node
- DevOps
- Full Stack
- Wordpress

## :three: TELEPRO
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/telepro.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***TELEPRO***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][tres] 

[tres]: https://telepro.com.mx

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google2].

[google2]: https://goo.gl/maps/iD9FueWmFoGvhiBJA

### :large_blue_circle: **`Acerca de:`** :question:
> Telepro es una empresa multinacional desarrolladora de soluciones basadas en tecnología de la información (software), establecida desde 1995
Valores de Telepro:
- Honestidad y respeto
- Responsabilidad
- Trabajo en equipo
- Lealtad
- Innovacion 
- Confianza
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Administración de portafolios de carteras de crédito y arrendamiento
- Desarrollo de Soluciones Paramétricas
- Implementación de Soluciones en Muy Corto Tiempo
- Control y Automatización de Procesos de Originación de Crédito y Arrendamiento
- Solución Móvil Self Services
- Inteligencia Artificial (AI)
- Módelos de Riesgo y Comportamiento de Pago
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/InfoTelepro?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/servicios-telepro-s.a.-de-c.v./

:small_orange_diamond: **Correo:** info@telepro.com.mx

### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de telepro, se pueden encontrar en los siguientes enlaces:
- https://telepro.com.mx/desarrollador-jr.html
- https://telepro.com.mx/nosotros.html
### :large_blue_circle: **`Blog:`** :pencil:
El blog de telepro para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.linkedin.com/company/servicios-telepro-s.a.-de-c.v./
### :large_blue_circle: **`Tecnologia:`** :calling:
- ASP.Net C# 
- Javascript 
- AJAX 
- CSS 
- HTML5 
- SQL Server 
- Frameworks 
- web services
- API Rest
- Bootstrap
- Crystal Reports 



## :four: SVITLA
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/svitla.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***SVITLA***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][cuatro] 

[cuatro]: https://svitla.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google3].

[google3]: https://goo.gl/maps/g9qguWeaCH8XKzGA6

### :large_blue_circle: **`Acerca de:`** :question:
> Svitla Systems es una empresa multinacional de desarrollo de software con sede en Silicon Valley, con oficinas comerciales y de desarrollo en los EE. UU., México y Europa (Ucrania, Polonia, Alemania). Desde 2003, Svitla ha prestado servicios a una amplia gama de clientes que van desde nuevas empresas innovadoras en Silicon Valley hasta grandes corporaciones como Amplience, Ingenico, BMC y AstraZeneca. Nuestros proyectos representan las industrias más avanzadas y provienen de todo el mundo. Si está buscando ampliar su equipo técnico rápidamente, somos su socio glocal de confianza. Usamos nuestro modelo de extensión de equipo patentado, donde nuestros desarrolladores y gerentes de proyecto experimentados se convierten en una extensión natural del equipo del cliente. Al trabajar con los clientes directamente y al estar completamente integrados con los procesos del equipo en el sitio, nuestros desarrolladores crean asociaciones duraderas y exitosas y crean los productos más interesantes
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo de software
- Desarrollo de aplicaciones
- Desarrollo web personalizado
- Outsourcing de TI, equipo de desarrollo dedicado
- Big Data, Internet de las cosas
- Desarrollo web, desarrollo móvil y eCommerce
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/SvitlaSystems?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/svitla-systems-inc-/

:small_orange_diamond: **Youtube** https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA

:small_orange_diamond: **Instagram**  https://www.instagram.com/svitlasystems/

:small_orange_diamond: **Twitter** https://twitter.com/SvitlaSystemsIn
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de svitla, se pueden encontrar en el siguiente enlace:
https://svitla.com/career
### :large_blue_circle: **`Blog:`** :pencil:
El blog de svitla para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://svitla.com/blog
### :large_blue_circle: **`Tecnologia:`** :calling:
- Java
- Frontend 
- Fullstack
- Node
- React
- Python
- Rybu
- Aws


## :five: ATALAIT
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/atalait.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***ATALAIT***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][cinco] 

[cinco]: https://www.atalait.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google4].

[google4]: https://goo.gl/maps/BJtuTirATLeW3gv8A

### :large_blue_circle: **`Acerca de:`** :question:
> Atalait es la plataforma que conecta requerimientos estratégicos de negocio que pueden ser solventados mediante soluciones tecnológicas, permitiendo a las organizaciones enfocarse en su relevancia en el mercado, a través de la Operación, Innovación y Protección.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Centro Alterno de Trabajo
- Plan de recuperación ante desastres
- Servicios administrados de continuidad
- Consultoría de continuidad de negocio
- Software de gestión del programa de gestión del negocio
- Technology Sharing
- Soluciones tecnológicas, DRP y DRaaS
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/Atalait-594842997338059?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/atalait/about/

:small_orange_diamond: **Instagram**  https://www.instagram.com/atalait_adn/

:small_orange_diamond: **Twitter** https://twitter.com/Atalait
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de atalait, se pueden encontrar en el siguiente enlace:
 https://www.atalait.com/bolsa-de-trabajo/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de atalait para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.atalait.com/blog/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Java
- JScript
- HTML
- JSon
- SpringBoot
- JBoos




## :six: ENSITECH
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/Ensitech.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***ENSITECH***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][seis] 

[seis]: https://ensitech.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google5].

[google5]: https://goo.gl/maps/1eGmGZiRFqxZxD689

### :large_blue_circle: **`Acerca de:`** :question:
> Ayudamos a las empresas a ganar el mercado y convertir sus ideas en negocios exitosos habilitando soluciones de software de alto valor. Somos un equipo de ingenieros, diseñadores, analistas de negocios y científicos de datos enfocados en ayudar a gobiernos y empresas a ser más ágiles y conectados con sus mercados mediante el uso de tecnologías de nube, móviles y de big data
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Aumento de personal 
- Desarrollo de software 
- Operaciones en la nube 
- Big Data Analytics 
- Inteligencia artificial
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/EnsitechMx/?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/ensitech/

:small_orange_diamond: **Instagram**  https://www.instagram.com/ensitech_mx/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de ensitech, se pueden encontrar en el siguiente enlace:
 https://ensitech.com/careers/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de ensitech para encontrar sus publicaciones se puede encontrar en los siguientes enlaces:
- https://ensitech.com/infographics/
- https://ensitech.com/case-study/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Desarrollo .net
- Fullstack
- Lanza libre
- Frontend




## :seven: ENKODER
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/Enkoder.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***ENKODER***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][siete] 

[siete]: https://enkoder.com.mx/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google6].

[google6]: https://goo.gl/maps/DdicmzY5YfZ5nu7Q9

### :large_blue_circle: **`Acerca de:`** :question:
> Desarrolladores de software ubicados en México con alto nivel de experiencia que se adaptarán rápidamente a las necesidades de su negocio.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Sistema de gestión de pedidos
- Sistema de seguimiento de entregas
- Administración de la tienda
- Facturacion electronica
- Almacenamiento de datos
- Gamificación
- Comercio electrónico
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/Enkoder-Software-Developers-654625184960733/?ref=nearby_places&_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/enkoder/

:small_orange_diamond: **Twitter**  https://twitter.com/EnkoderIT
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de enkoder, se pueden encontrar en el siguiente enlace:
 https://www.linkedin.com/company/enkoder/jobs/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de enkoder para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.linkedin.com/company/enkoder/posts/?feedView=all
### :large_blue_circle: **`Tecnologia:`** :calling:
:small_orange_diamond: **Lenguages**
- C#
- PHP
- Objetive C
- Python
- Java

:small_orange_diamond: **Frameworks**
- Angular
- Xamarin
- Vue
- .Net Core

:small_orange_diamond: **Databases**
- MySql
- DynamoDB
- CosmosDB

:small_orange_diamond: **Consulting**
- eCommerce
- DevOps
- Cloud Computing




## :eight: ESTRASOL
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/Estrasol.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***ESTRASOL***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][ocho] 

[ocho]: https://estrasol.com.mx/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google7].

[google7]: https://goo.gl/maps/2mFuPdEzG94LZmL38

### :large_blue_circle: **`Acerca de:`** :question:
> Estrasol es una empresa sólida que ofrece una estrategia de solución de 360º con unidades de negocio especializada en Consultoría, Desarrollo de Software y Marketing DigitalNuestros clientes nos ven como una extensión del área TI de sus empresas.Te ofrecemos la posibilidad de tener la información de tu compañía en tiempo real, el control adecuado de tus ventas que permita un crecimiento constante, y la difusión necesaria para que tu marca sea aún más reconocida.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Consultoría
- Desarrollo de Software 
- Marketing Digital
- Aplicaciones móviles para IOS y Android.
- Diseño, fabricación e implementación de soluciones de kioscos electrónicos multiservicio para gobiernos y empresas.
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/Estrasol/?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/estrasol/

:small_orange_diamond: **Twitter**  https://twitter.com/estrasol_

:small_orange_diamond: **Behance** https://www.behance.net/estrasol
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de estrasol, se pueden encontrar en el siguiente enlace:
https://estrasol.com.mx/unete-a-estrasol
### :large_blue_circle: **`Blog:`** :pencil:
El blog de estrasol para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
 https://estrasol.com.mx/blog
### :large_blue_circle: **`Tecnologia:`** :calling:
- Ruby on Rails
- Java
- .net 
- Php




## :nine: SIMPAT TECH
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/simpat.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***SIMPAT TECH***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][nueve] 

[nueve]:https://simpat.tech

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google8].

[google8]: https://goo.gl/maps/qrryf93v3BHy5a3UA

### :large_blue_circle: **`Acerca de:`** :question:
> Simpat Tech ofrece a los líderes de TI con visión de futuro excelencia técnica de principio a fin, así como la información estratégica necesaria para alinear las prioridades de desarrollo con los objetivos comerciales de alto nivel. 
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo, implementación, recursos y consultoría de software contratado y bajo demanda para organizaciones de todo tipo y tamaño.
- Servicios de consultoría 
- Mejora del equipo que mejoran el desempeño organizacional al tiempo que brindan a los clientes una mayor flexibilidad y reducen su riesgo.
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/simpat.tech?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/simpattech/

:small_orange_diamond: **Twitter**  https://twitter.com/simpat_tech

:small_orange_diamond: **Instagram** https://www.instagram.com/simpat.tech/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de simpat tech, se pueden encontrar en el siguiente enlace:
https://simpat.tech/careers/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de simpat tech para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://simpat.tech/insights/
### :large_blue_circle: **`Tecnologia:`** :calling:
:small_orange_diamond: **Custom Software**
- Microsoft Stack
- .NET Core
- ASP
- .NET MVC
- API
- SQL
- Entity framework
- C#

:small_orange_diamond: **DevOps**
- CI/CD
- including Azure pipeline
- Kubernetes
- Docker
- Octopus

:small_orange_diamond: **Cloud Solutions**
- Azure 
- AWS 
- Kubernetes 
- Docker 
- PAaS 
- RabbitMQ 




## :ten: AVIADA
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/aviada.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***AVIADA***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][diez] 

[diez]: https://aviada.mx/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google9].

[google9]: https://goo.gl/maps/aYN1C6ETWhw4RyQz5

### :large_blue_circle: **`Acerca de:`** :question:
> Aviada es una empresa de nearshoring y offshoring en México para todas sus necesidades de aumento de personal. Nos especializamos en encontrar y cuidar a grandes empleados en los campos de desarrollo de software, medios digitales, diseño multimedia y otros.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Recursos de TI
- Desarrollo de software
- Control de calidad
- Dev Ops
- Tech Ops
- Ingenieros de software y Servicios al cliente
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/aviadamx

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/aviadamx/

:small_orange_diamond: **Twitter**  https://twitter.com/aviadamx

:small_orange_diamond: **Instagram** https://www.instagram.com/aviadamx/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de aviada, se pueden encontrar en los siguientes enlaces:
- https://aviada.mx/trabaja-con-aviada/
- https://careers.aviada.mx/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de aviada para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://aviada.mx/category/blog/
### :large_blue_circle: **`Tecnologia:`** :calling:
- QA
- Project Management
- UI/UX
- AdOps





## :one: :one: STARTUP MEXICO
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/startup.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***STARTUP***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][once] 

[once]: https://www.startupmexico.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google10].

[google10]: https://goo.gl/maps/CRmMCyEx4TtA5Vbb7

### :large_blue_circle: **`Acerca de:`** :question:
> Es la organización líder en México en la promoción de la innovación, la cultura emprendedora y el desarrollo económico tanto a nivel local como a nivel internacional. Apoyamos Startups prometedoras de alto impacto, proporcionándoles espacio físico, mentores, talleres, financiación y un ecosistema de servicios profesionales críticos. Nuestra metodología probada permite a cada empresa operar con eficiencia y eficacia para mejorar su probabilidad de éxito.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Startup Incubation
- Startup Acceleration
- Coworking Space y Investment / Funding
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/SUMstartupmexico/?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/startupmexico/

:small_orange_diamond: **Twitter** https://twitter.com/startupmexico

:small_orange_diamond: **Instagram**https://www.instagram.com/startup_mexico/

:small_orange_diamond: **Youtube** https://www.youtube.com/channel/UCtihaSsjmL1Ofqj7y3600OA
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de startup, se pueden encontrar en el siguiente enlace:
https://www.startupmexico.com/acerca-de-nosotros/sumate
### :large_blue_circle: **`Blog:`** :pencil:
El blog de startup para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.startupmexico.com/blogs_es/blog-de-sum
### :large_blue_circle: **`Tecnologia:`** :calling:
- Startup Incubation
- Startup Acceleration
- Coworking Space y Investment / Funding




## :one: :two: INTELIMETRICA
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/intelime.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***INTELIMETRICA***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][doce] 

[doce]: https://intelimetrica.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google11].

[google11]: https://goo.gl/maps/DKsLnsYtdWEPcR9a7

### :large_blue_circle: **`Acerca de:`** :question:
> Intelimétrica es una empresa que se dedica a crear productos basados en datos y plataformas inteligentes. Nuestro portafolio incluye clientes tales como bancos y empresas de clase mundial y también ofrecemos soluciones para el público en general. Actualmente estamos revolucionando el mercado inmobiliario aplicando una filosofía de análisis intensivo de datos y utilizando algoritmos para democratizar el acceso a la información y facilitar la toma de decisiones, en una de las industrias más importantes, pero menos modernizada en el mundo.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- IA y ML
- Ingenieria de software 
- Diseño Ux
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/intelimetrica?_rdc=1&_rdr

:small_orange_diamond: **Linkedin:** https://www.linkedin.com/company/aviadamx/

:small_orange_diamond: **GitHub**  https://github.com/Intelimetrica
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de intelimetrica, se pueden encontrar en el siguiente enlace:
https://intelimetrica.com/careers
### :large_blue_circle: **`Blog:`** :pencil:
El blog de intelimetrica para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://intelimetrica.com/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Software
- Data 
- Web
 



## :one: :three: MICHELADA
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/michelada.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***MICHELADA***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][trece] 

[trece]: https://www.michelada.io/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google12].

[google12]: https://goo.gl/maps/6Wd5vsygWcHmGHoZ6

### :large_blue_circle: **`Acerca de:`** :question:
> Planificamos, codificamos y lanzamos increíbles productos web y móviles. Desarrollamos experiencias de alto rendimiento para web, aplicaciones y vida. Mezclamos sus ideas con tecnologías probadas para lograr sus objetivos comerciales.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
> Desarrollamos software para soluciones interactivas y picantes. Ya sea que sea una empresa nueva o una corporación, podemos ayudarlo a transformar grandes ideas en software utilizable para productos atractivos.
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/micheladaio?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/micheladaio

:small_orange_diamond: **GitHub**  https://github.com/michelada
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de michelada, se pueden encontrar en el siguiente enlace:
https://jobs.michelada.io/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de michelada para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.linkedin.com/company/michelada-io/posts/?feedView=all
### :large_blue_circle: **`Tecnologia:`** :calling:
- Sistemas unix/Linux
- PostgreSQL
- AWS
- Azure
- DNS
- Docker
- Kubernets
- JavaScript
- CSS
- Front-end
- Angular






## :one: :four: SERTI
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/serti.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***SERTI***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][catorce] 

[catorce]: https://serti.com.mx/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google13].

[google13]: https://goo.gl/maps/BSNxCAJ6tvVRqNfk9

### :large_blue_circle: **`Acerca de:`** :question:
> Somos una empresa con mentalidad de innovación y servicio, generamos soluciones de software en tecnologías de información que ayudan al crecimiento de nuestros clientes.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo  de plataformas web
- Desarrollo grafico
- Desarrollo de aps
- Reclutamiento TI
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/SERTIMX/?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/serTI_Mx

:small_orange_diamond: **Linkedin**   https://www.linkedin.com/company/servicios-estrat-gicos-en-tecnolog-as-de-informaci-n/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de serti, se pueden encontrar en el siguiente enlace:
https://serti.com.mx/bolsa-de-trabajo/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de serti para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.linkedin.com/company/servicios-estrat-gicos-en-tecnolog-as-de-informaci-n/posts/?feedView=all
### :large_blue_circle: **`Tecnologia:`** :calling:
- Java.
- Javascript.
- Go.
- Kotlin.
- Python.
- Lenguaje C / C++
- Scala.
- Ruby.






## :one: :five: ENROUTE
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/enroute.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***ENROUTE***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][quince] 

[quince]: https://enroutesystems.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google14].

[google14]: https://goo.gl/maps/Gakuqd8Gj8wD6PWo9

### :large_blue_circle: **`Acerca de:`** :question:
> Nuestra misión: Desafío implacable del Status Quo mediante el uso de la tecnología haciendo lo que amamos y con un enfoque en hacer felices y exitosos a nuestros clientes.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
Brindamos servicios y soluciones de TI proporcionados por un equipo de personas apasionadas en la resolución de problemas, altamente capacitadas en diferentes prácticas comerciales y de TI.
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/enroutesystems?_rdc=1&_rdr

:small_orange_diamond: **Instagram:** https://www.instagram.com/enroutesystems/

:small_orange_diamond: **Linkedin**   https://www.linkedin.com/company/enroutesystems/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de enroutesystems, se pueden encontrar en el siguiente enlace:
https://enroutesystems.com/careers
### :large_blue_circle: **`Blog:`** :pencil:
El blog de enroutesystems para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://enroutesystems.com/case-studies
### :large_blue_circle: **`Tecnologia:`** :calling:
- AWS
- Python
- Jenkins
- Java
- Swift
- Scala
- Spring
- Terraform




## :one: :six: IMPROVING
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/improving.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***IMPROVING***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][dieciseis] 

[dieciseis]: https://www.itexico.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google15].

[google15]: https://goo.gl/maps/F6jMad9VUooy91xs6

### :large_blue_circle: **`Acerca de:`** :question:
> Improving Nearshore es una unidad de negocios de servicios digitales, subsidiaria de Improving, que ayuda a empresas emergentes, en crecimiento, medianas y empresariales a innovar en sus negocios. Integramos las mejores prácticas de equipos ágiles en cómo necesita la innovación digital e implementamos las mejores prácticas de entrega de servicios empresariales con centros de innovación en México.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo de software
- Diseño de Producto Digital UI / UX
- Desarrollo de front-end
- Desarrollo de back-end
- Servicios de control de calidad y pruebas
- Desarrollo tecnológico de Microsoft
- Desarrollo de tecnología Java
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:**  https://web.facebook.com/itexico?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/iTexico

:small_orange_diamond: **Linkedin**   https://www.linkedin.com/showcase/improving-nearshore/

:small_orange_diamond: **Youtube:**  https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de improving, se pueden encontrar en el siguiente enlace:
https://enroutesystems.com/careers
### :large_blue_circle: **`Blog:`** :pencil:
El blog de improving para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.itexico.com/blog
### :large_blue_circle: **`Tecnologia:`** :calling:
- Angular
- Cloud
- CI / CD
- Databases
- Kafka
- Restful
- Java
- Python


## :one: :seven: GRUPOGFT
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/grupogft.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***GRUPOGFT***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][diecisiete] 

[diecisiete]: https://www.gft.com/mx/es/index/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google16].

[google16]: https://goo.gl/maps/vD4fjAXxagYJ6WBdA

### :large_blue_circle: **`Acerca de:`** :question:
> GFT Technologies SE (GFT) es una consultora de negocio y tecnología en la que confían instituciones financieras líderes a nivel mundial para resolver sus retos de negocio. GFT da respuestas al constante cambio regulatorio actual e innova continuamente para satisfacer las demandas de la revolución digital. GFT aúna el asesoramiento, la creatividad y la tecnología con la innovación y el conocimiento especializado del sector financiero para transformar los negocios de sus clientes.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Inteligencia artificial
- Cloud
- Analisis de datos
- Greencoding
- Blockchain y dlt
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:** https://web.facebook.com/gft.mex?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/gft_mx

:small_orange_diamond: **Instagram**   https://www.instagram.com/gft_tech/

:small_orange_diamond: **Youtube:**  https://www.youtube.com/user/gftgroup
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de grupogft, se pueden encontrar en el siguiente enlace:
https://jobs.gft.com/Mexico/go/mexico/4412401/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de grupogft para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.gft.com/mx/es/index/descubrir/innovation-at-gft/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Dominio Java 8
- Microservicios
- Spring 5, Spring boot, Spring cloud
- Git
- Junit, Mockito, Assertj, Pruebas de Integración y Cobertura
- WebServices Soap y Rest

## :one: :eight: NOVA
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/nova.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***NOVA***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][diesiocho] 

[diesiocho]: http://novasolutionsystems.com/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google17].

[google17]: https://goo.gl/maps/iEuSkgBbHH2RVGkr7

### :large_blue_circle: **`Acerca de:`** :question:
> Somos consultores competitivos técnicamente, con iniciativa, curiosidad y espíritu emprendedor. Nos adaptamos, colaboramos y respondemos a las necesidades del cliente, al entregar lo mejor de nosotros en cada proyecto.
Todo lo que aprendemos, lo mejoramos, reforzamos y lo compartimos. Nuestra base esencial es la comunicación: nosotros escuchamos, analizamos y retroalimentamos.
tes.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Recursos digitales
- Seguridad financiera
- Transformación digital 
- Estrategia digital
- Fintech Factory 
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:** https://web.facebook.com/novasolutionsystems/?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/novasolutionsys/

:small_orange_diamond: **Linkedin**  https://www.linkedin.com/company/novasolutionsystems/
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de nova, se pueden encontrar en el siguiente enlace:
http://novasolutionsystems.com/unete/#vacantes
### :large_blue_circle: **`Blog:`** :pencil:
El blog de nova para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.linkedin.com/company/novasolutionsystems/posts/?feedView=all
### :large_blue_circle: **`Tecnologia:`** :calling:
- SQL O PL/SQL
- Phyton	
- Data Mining
- machine learning
- Git
- Gitlab
- Bitbucket
- Java

## :one: :nine: SCIO
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/scio.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***SCIO***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][diecinueve] 

[diecinueve]: https://www.scio.com.mx/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google18].

[google18]: https://goo.gl/maps/2fuTtGEwEbCUKcsN9

### :large_blue_circle: **`Acerca de:`** :question:
> Somos una empresa de desarrollo de software a la medida con más de 15 años de experiencia en el mercado.
Nuestro principal objetivo es hacer que tu te centres en lo realmente importante para tu negocio y nosotros nos encargamos de tu software.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo de aplicaciones
- Desarrollo de aplicaciones de negocio
- Consultoria tecnológica
- Soluciones de Software
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Facebook:** https://web.facebook.com/ScioMx?_rdc=1&_rdr

:small_orange_diamond: **Twitter:** https://twitter.com/sciomx
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de scio, se pueden encontrar en el siguiente enlace:
https://www.scio.com.mx/trabaja-scio-mexico/
### :large_blue_circle: **`Blog:`** :pencil:
El blog de scio para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://www.scio.com.mx/blog/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Software Business Analyst/Product Owner
- Senior NodeJS Developer
- Senior Front End Developer (JavaScript)
- Full Stack Developer .NET
- Front End Developer – ReactJS

## :two: :zero: APTUDE
### :large_blue_circle: **`Logotipo`**:

![Una imagen](imagenes/aptude.jpg)

### :large_blue_circle: **`Nombre:`**  :pencil2:
***APTUDE***
### :large_blue_circle: **`Sitio Web:`**  :earth_asia:
[CLICK PARA IR AL SITIO WEB:][veinte] 

[veinte]: https://aptude.com/es/

### :large_blue_circle: **`Ubicación:`** :pushpin:
La empresa se encuentra ubicada en:
[Ir a la ubicacion en google maps][google19].

[google19]: https://goo.gl/maps/qJ8eRuV5AdHq9dHQ6

### :large_blue_circle: **`Acerca de:`** :question:
> Aptude es una empresa de consultoría de TI incorporada hace veinte años en 2000. Nuestro modelo de negocio tiene dos enfoques distintos que se combinan para satisfacer las diversas necesidades de nuestros clientes.
### :large_blue_circle: **`Servicios:`** :hammer: :wrench:
- Desarrollo de aplicaciones
- Inteligencia de negocio
- Consultoria TI
- Aplicaciones móviles
- Servicios de Apoyo
### :large_blue_circle: **`Presencia:`** :house:
:small_orange_diamond: **Email:**  info@aptude.com
### :large_blue_circle: **`Ofertas Laborales:`** :money_with_wings:
Las ofertas laborales de aptude no se pueden encontrar.
### :large_blue_circle: **`Blog:`** :pencil:
El blog de aptude para encontrar sus publicaciones se puede encontrar en el siguiente enlace:
https://aptude.com/es/blog/
### :large_blue_circle: **`Tecnologia:`** :calling:
- Spark
- Php
- Python
- Aws
- Node
- Oracle
- Java
- Js
- Angular 
- MySql























